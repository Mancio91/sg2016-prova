package core;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.junit.Test;


public class ReadGitRepositoryTest {

	private final File repoPath = new File("sample_repos/sample01");
	private final GitRepository repository = new GitRepository(repoPath);
	
	//trovare da shell l'hash del commit relativo al master del repository in sample_repos/sample01
	private static String masterCommitHash = "1e7c193371b42215bc9ba653c48f58ebbb1a7aae"; 

	@Test
	public void shouldFindHead() throws Exception {
		assertEquals("refs/heads/master",repository.getHeadRef());
	}

	@Test
	public void shouldFindHash() throws Exception {
		assertEquals(masterCommitHash,repository.getRefHash("refs/heads/master"));
	}
}
