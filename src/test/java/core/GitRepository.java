package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class GitRepository {

	private File repoPath;
	
	public GitRepository(File repoPath) {
		this.repoPath = repoPath;
	}

	private File[] getFilesByDir(String name) {
		
		File dir = new File(name);
		if(!dir.isDirectory())
			return null;
		
		File[] res = null;
		int i = 0;
		
		for(File f : dir.listFiles())
			i++;
		res = new File[i];
		
		i = 0;
		for(File f : dir.listFiles()) {
			res[i] = f;
			i++;
		}	
		return res;
	}
	
	public String getHeadRef() {
		
		String res = null;
		
		for (File f : repoPath.listFiles()) {
			if(f.getName().equals("HEAD")) {
				try {
					BufferedReader reader = new BufferedReader(new FileReader(f));
					String line = reader.readLine();
					res = line.split(":")[1].trim();
					break;
					
				} catch (FileNotFoundException e) {					
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}		
			}
		}
		
		return res;
	}

	public String getRefHash(String string) {
		
		String res = null;
		File f = new File(this.repoPath + "/" + string);
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			res = reader.readLine().trim();
			
		} catch (FileNotFoundException e) {					
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		return res;
	}

}
